# PHP Keep-It-Simple Framework

A simple and file base routing web framework in php.

## Features

- File base routing
- External package management using composer
- Built-in docker configuration for local development
