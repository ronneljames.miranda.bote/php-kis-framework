<?php

namespace Core\Contracts;

interface Renderer
{
    /**
     * @param  array<int,mixed>  $data
     */
    public function render(string $file, array $data): string;
}
