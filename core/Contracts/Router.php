<?php

namespace Core\Contracts;

use Core\Http\Response;

interface Router
{
    public function response(string $uri): Response;
}
