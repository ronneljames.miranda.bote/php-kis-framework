<?php

namespace Core;

use Core\Contracts\Renderer;
use Core\Contracts\Router;

final class Application
{
    private static Application $instance;

    private Container $container;

    private ?Router $router;

    private ?Renderer $renderer;

    final private function __construct(Container $container)
    {
        $this->container = $container;
    }

    public static function instance(): self
    {
        if (! isset(self::$instance)) {
            self::$instance = new self(Container::instance());
        }

        return self::$instance;
    }

    /**
     * @param  class-string  $name
     */
    public function getService(string $name)
    {
        return $this->container->service($name);
    }

    /**
     * @param  class-string  $class
     */
    public function addService(string $name, string $class, mixed ...$configuration): void
    {
        $this->container->add($name, $class, ...$configuration);
    }

    public function setRenderer(string $class, mixed ...$configuration): void
    {
        $this->addService(Renderer::class, $class, ...$configuration);

        $this->renderer = null;
    }

    public function setRouter(string $class, mixed ...$configuration): void
    {
        $this->addService(Router::class, $class, ...$configuration);

        $this->router = null;
    }

    public function renderer(): Renderer
    {
        if (! $this->renderer) {
            $this->renderer = self::$instance->getService(Renderer::class);
        }

        return $this->renderer;
    }

    public function router(): Router
    {
        if (! $this->router) {
            $this->router = self::$instance->getService(Router::class);
        }

        return $this->router;
    }
}
