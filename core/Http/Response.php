<?php

namespace Core\Http;

class Response
{
    private string $content;

    private array $headers = [];

    private int $statusCode;

    public function __construct(string $content)
    {
        $this->content = $content;
    }

    public function addHeader(string $name, string $value): self
    {
        $this->headers[$name] = $value;

        return $this;
    }

    public function statusCode(int $statusCode): self
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    public function send(): string
    {
        foreach ($this->headers as $key => $value) {
            header($key.':'.$value);
        }

        http_response_code($this->statusCode);

        return $this->content;
    }
}
