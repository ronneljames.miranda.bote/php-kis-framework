<?php

namespace Core;

final class Container
{
    private static Container $instance;

    private static array $services = [];

    final private function __construct()
    {
    }

    public static function instance(): self
    {
        if (! isset(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param  class-string  $class
     */
    public function add(string $name, string $class, mixed ...$configuration): void
    {
        self::$services[$name] = new $class(...$configuration);
    }

    /**
     * @param  class-string  $name
     */
    public function service(string $name)
    {
        if (! isset(self::$services[$name])) {
            throw new \RuntimeException('Service not found');
        }

        return self::$services[$name];
    }
}
