<?php

use Carbon\Carbon;
use Dotenv\Dotenv;

$rootDir = implode(DIRECTORY_SEPARATOR, [__DIR__, '..']);

require_once implode(DIRECTORY_SEPARATOR, [$rootDir, 'vendor', 'autoload.php']);

$dotEnv = Dotenv::createImmutable($rootDir);
$dotEnv->load();

$database = new $_ENV['DB_LIBRARY'](
    $_ENV['DB_HOST'],
    $_ENV['DB_DATABASE'],
    $_ENV['DB_USERNAME'],
    $_ENV['DB_PASSWORD'],
    $_ENV['DB_PORT'],
);

function runMigrations($database, $basePath)
{
    if (! count($database->query("SHOW TABLES LIKE 'migrations'"))) {
        $database->query(
            'CREATE TABLE migrations (id INT UNSIGNED NOT NULL AUTO_INCREMENT, name VARCHAR(120) NOT NULL, date_created DATETIME NOT NULL, PRIMARY KEY (id))',
        );
    }

    $executed = array_column($database->query('SELECT name FROM migrations'), 'name');

    $migrationDir = (implode(DIRECTORY_SEPARATOR, [$basePath, 'database', 'migrations']));

    $migrations = scandir($migrationDir);

    $migrations = array_filter($migrations, function ($file) use ($executed) {
        return ! preg_match('/^\./', $file) && ! in_array($file, $executed);
    });

    if (! count($migrations)) {
        echo "Nothing to migrate.\n";

        return;
    }

    $migrations = array_map(function ($file) {
        $classname = preg_replace('/^[0-9]*_/', '', $file);

        $classname = str_replace(['_', '.php'], '', ucwords($classname, '_'));

        return [
            $file,
            $classname,
        ];
    }, $migrations);

    echo "Database migration starting...\n\n";

    foreach ($migrations as $migrationFile) {
        require_once implode(DIRECTORY_SEPARATOR, [$migrationDir, $migrationFile[0]]);

        $migrator = new $migrationFile[1]($database);

        $migrator->execute();

        echo "Success: $migrationFile[0] \n";

        $database->query(
            'INSERT INTO migrations (name, date_created) VALUES (:name, :date_created)',
            ['name' => $migrationFile[0], 'date_created' => Carbon::now()->toDateTimeString()]
        );
    }

    echo "\nDatabase finished.\n";
}

runMigrations($database, basePath());

$database = null;

exit();
