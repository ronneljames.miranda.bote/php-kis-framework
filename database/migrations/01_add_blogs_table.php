<?php

class AddBlogsTable
{
    private $database;

    public function __construct($database)
    {
        $this->database = $database;
    }

    public function execute(): void
    {
        $this->database->query(
            'CREATE TABLE blogs (
                id INT UNSIGNED NOT NULL AUTO_INCREMENT,
                title VARCHAR(120) NOT NULL,
                date_created DATETIME NOT NULL,
                PRIMARY KEY (id)
            )',
        );
    }
}
