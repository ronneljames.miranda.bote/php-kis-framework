<?php

$rootDir = implode(DIRECTORY_SEPARATOR, [__DIR__, '..']);

require_once implode(DIRECTORY_SEPARATOR, [$rootDir, 'vendor', 'autoload.php']);

$app = require_once implode(DIRECTORY_SEPARATOR, [$rootDir, 'bootstrap.php']);

$response = $app->router()->response($_SERVER['REQUEST_URI']);

echo $response->send();

exit();
