<?php

use App\Http\FileLoaderRouter;
use App\View\TwigRenderer;
use Core\Application;
use Dotenv\Dotenv;

$dotEnv = Dotenv::createImmutable(__DIR__);
$dotEnv->load();

$app = Application::instance();

$resourcesDir = implode(DIRECTORY_SEPARATOR, [__DIR__, 'resources']);

$app->setRouter(
    FileLoaderRouter::class,
    implode(DIRECTORY_SEPARATOR, [$resourcesDir, 'pages'])
);

$app->setRenderer(
    TwigRenderer::class,
    implode(DIRECTORY_SEPARATOR, [$resourcesDir, 'templates']),
    false
);

$app->addService(
    'database',
    $_ENV['DB_LIBRARY'],
    $_ENV['DB_HOST'],
    $_ENV['DB_DATABASE'],
    $_ENV['DB_USERNAME'],
    $_ENV['DB_PASSWORD'],
    $_ENV['DB_PORT'],
);

return $app;
