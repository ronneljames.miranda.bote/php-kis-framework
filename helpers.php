<?php

use Core\Application;
use Core\Http\Response;

if (! function_exists('basePath')) {
    function basePath(): string
    {
        return __DIR__;
    }
}

if (! function_exists('app')) {
    function app(): Application
    {
        return Application::instance();
    }
}

if (! function_exists('response')) {
    function response(string $content): Response
    {
        return new Response($content);
    }
}

if (! function_exists('renderView')) {
    /**
     * @param  array<int,mixed>  $data
     */
    function renderView(string $name, array $data = []): string
    {
        return app()->renderer()->render($name, $data);
    }
}
