<?php

namespace App\Http;

use Core\Contracts\Router;
use Core\Http\Response;

final class FileLoaderRouter implements Router
{
    private string $pagesDirectory;

    public function __construct(string $pagesDirectory)
    {
        $this->pagesDirectory = rtrim($pagesDirectory, DIRECTORY_SEPARATOR);
    }

    public function response(string $uri): Response
    {
        $result = $this->getFile($uri);

        if (! $result) {
            throw new \RuntimeException('404 - Page Not found');
        }

        $response = require_once $result['file'];

        if (! is_callable($response)) {
            throw new \RuntimeException('Page file should return a closure');
        }

        return $response($result['param']);
    }

    private function getFile(string $uri): ?array
    {
        $uri = rtrim($uri, '/');
        $uri = $uri === '' ? '/' : $uri;

        $file =
            implode(
                DIRECTORY_SEPARATOR,
                [$this->pagesDirectory, str_replace('/', DIRECTORY_SEPARATOR, trim($uri, '/'))]
            );

        $file = rtrim($file, DIRECTORY_SEPARATOR);

        if ($exactFile = $this->getIfExactFile($file)) {
            return ['file' => $exactFile, 'param' => null];
        }

        if ($indexFile = $this->getIfIndexFile($file)) {
            return ['file' => $indexFile, 'param' => null];
        }

        if ($dynamicFile = $this->getIfDynamicFile($file)) {
            return ['file' => $dynamicFile, 'param' => array_slice(explode(DIRECTORY_SEPARATOR, $file), -1)[0] ?? null];
        }

        return null;
    }

    private function getIfExactFile(string $file): ?string
    {
        if (file_exists($file.'.php')) {
            return $file.'.php';
        }

        return null;
    }

    private function getIfIndexFile(string $file): ?string
    {
        if (is_dir($file)) {
            $indexFile = null;

            foreach (scandir($file) as $content) {
                if ($content === 'index.php') {
                    $indexFile = implode(DIRECTORY_SEPARATOR, [$file, $content]);
                }

                unset($content);
            }

            if ($indexFile) {
                return $indexFile;
            }
        }

        return null;
    }

    private function getIfDynamicFile(string $file): ?string
    {
        $fileDisected = explode(DIRECTORY_SEPARATOR, $file);

        $previousDir = implode(DIRECTORY_SEPARATOR, array_slice($fileDisected, 0, count($fileDisected) - 1));

        if (is_dir($previousDir)) {
            $previousDirContents = scandir($previousDir);

            $dynamicFile = null;

            foreach ($previousDirContents as $content) {
                if (preg_match('/^\[.*\].php$/i', $content)) {
                    $dynamicFile = implode(DIRECTORY_SEPARATOR, [$previousDir, $content]);
                }
            }

            if ($dynamicFile) {
                return $dynamicFile;
            }
        }

        return null;
    }
}
