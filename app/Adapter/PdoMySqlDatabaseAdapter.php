<?php

namespace App\Adapter;

use PDO;

class PdoMySqlDatabaseAdapter
{
    private PDO $instance;

    final public function __construct(string $host, string $database, string $username, string $password, int $port = 3306)
    {
        $this->instance = new PDO("mysql:host=$host;dbname=$database;port=$port", $username, $password);
    }

    /**
     * @param  array<string,mixed>  $values
     */
    public function query(string $query, array $values = [])
    {
        $prepare = $this->instance->prepare($query);

        $prepare->execute($values);

        return $prepare->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * @param  array<string,mixed>  $values
     */
    public function insert(string $query, array $values)
    {
        return $this->query($query, $values);
    }

    /**
     * @param  array<string,mixed>  $values
     */
    public function update(string $query, array $values)
    {
        return $this->query($query, $values);
    }

    /**
     * @param  array<string,mixed>  $values
     */
    public function delete(string $query, array $values): void
    {
        $this->query($query, $values);
    }
}
