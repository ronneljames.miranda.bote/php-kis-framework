<?php

namespace App\View;

use Core\Contracts\Renderer;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class TwigRenderer implements Renderer
{
    private Environment $renderer;

    public function __construct(string $templatePath, string|bool $cache)
    {
        $loader = new FilesystemLoader($templatePath);

        $this->renderer = new Environment($loader, ['cache' => $cache]);
    }

    public function render(string $file, array $data): string
    {
        return $this->renderer->render($file, $data);
    }
}
